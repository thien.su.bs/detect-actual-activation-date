## Requirements
- Python 3.7+

### Running the test

```console
python test_detect.py
```

Please read more in ```detect_actual.py```