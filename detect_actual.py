import datetime as dt
import csv
import operator

DATE_FORMAT = "%Y-%m-%d"


def sortable_date(x):
    item_first = operator.itemgetter(0)(x)
    item_first_dt = dt.datetime.strptime(item_first, DATE_FORMAT)
    return item_first_dt

def total_seconds_subtract_2_date(activation_date, deactivation_date):
    return (
        activation_date
        - dt.datetime.strptime(deactivation_date, DATE_FORMAT)
    ).total_seconds()
class DetectActualDate:
    def __init__(self, path_in, path_save):
        self.path_in = path_in
        self.path_save = path_save
        self.result = {}
        self.dict_process = {}

    def process_file_differently(self):
        """
        Process the file line by line using the file's returned iterator
        """
        path = self.path_in
        dict_process = {}
        try:
            with open(path) as file_handler:
                while True:

                    row = next(file_handler)
                    items_row = row.rstrip().split(",")
                    if (
                        operator.itemgetter(0)(items_row) == "PHONE_NUMBER"
                        or len(items_row) == 1
                    ):
                        row = next(file_handler)
                    else:
                        p_number = operator.itemgetter(0)(items_row)
                        if p_number in dict_process:
                            dict_process[p_number].append(
                                operator.itemgetter(1, 2)(items_row)
                            )
                        else:
                            dict_process[p_number] = [
                                operator.itemgetter(1, 2)(items_row)
                            ]

        except (IOError, OSError):
            print("Error opening / processing file")
        except StopIteration:
            return dict_process

    def write_csv(self):
        sorted_dict = {k: self.result[k] for k in sorted(self.result)}
        self.result = sorted_dict
        with open(self.path_save, mode="w") as output:
            writer = csv.writer(
                output, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL
            )

            writer.writerow(["PHONE_NUMBER", "REAL_ACTIVATION_DATE"])
            for row in self.result.items():
                writer.writerow(list(row))

    def process(self):
        result = {}
        for key, value in self.dict_process.items():
            list_value_sorted = sorted(value, key=sortable_date, reverse=True)
            date_stop = None
            len_list = len(list_value_sorted)
            # print(len_list)

            if len_list < 2:
                # print(value[0][0])
                result[key] = value[0][0]
                break
            for index in range(len_list):
                # print(list_value_sorted[index])
                item_tuple = list_value_sorted[index]
                activation_date = dt.datetime.strptime(item_tuple[0], DATE_FORMAT)
                deactivation_date = (
                    dt.datetime.strptime(item_tuple[1], DATE_FORMAT)
                    if item_tuple[1] != ""
                    else dt.datetime.now()
                )
                second_between = total_seconds_subtract_2_date(activation_date, list_value_sorted[index + 1][1])
                if second_between > 0.0:
                    # print("start at {}".format(list_value_sorted[index][0]))
                    real_activation = list_value_sorted[index][0]
                    result[key] = real_activation
                    break
                if len_list - index == 2:
                    # print("end")
                    # print(list_value_sorted[index + 1][0])
                    result[key] = list_value_sorted[index + 1][0]
                    break
        return result

    def call(self):
        self.dict_process = self.process_file_differently()
        self.result = self.process()
        self.write_csv()
