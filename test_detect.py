from io import StringIO
import csv
from detect_actual import *
import unittest
scsv = """PHONE_NUMBER,ACTIVATION_DATE,DEACTIVATION_DATE
0987000001,2016-03-01,2016-05-01
0987000002,2016-02-01,2016-03-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-12-01,
0987000002,2016-03-01,2016-05-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000002,2016-05-01,
0987000001,2016-06-01,2016-09-01
"""
scsv1 = """PHONE_NUMBER,ACTIVATION_DATE,DEACTIVATION_DATE
0987000001,2016-12-01,
0987000002,2016-05-01,
0987000001,2016-03-01,2016-05-01
0987000002,2016-02-01,2016-03-01
0987000001,2016-01-01,2016-03-01
0987000002,2016-03-01,2016-05-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-06-01,2016-09-01
"""
scsv2 = """PHONE_NUMBER,ACTIVATION_DATE,DEACTIVATION_DATE
0987000001,2016-03-01,2016-05-01
0987000002,2016-02-01,2016-03-01
0987000001,2016-12-01,
0987000002,2016-05-01,
0987000002,2016-03-01,2016-05-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
0987000003,2016-01-01,2016-01-10
0987000001,2016-09-01,2016-12-01
0987000001,2016-01-01,2016-03-01
0987000001,2016-06-01,2016-09-01
"""
class DetectActualCase(unittest.TestCase):
    def setUp(self):
        f = StringIO(scsv)
        with open("file_0.csv", mode="w") as f_Save:
            print(f.getvalue(), file=f_Save)

        path_in = r"file_0.csv"
        path_out = r"new_out_0.csv"
        
        f1 = StringIO(scsv1)
        with open("file1.csv", mode="w") as f_Save1:
            print(f1.getvalue(), file=f_Save1)
        path_in_1 = r"file2.csv"
        path_out_1 = r"new_out_1.csv"

        f2 = StringIO(scsv2)
        with open("file2.csv", mode="w") as f_Save2:
            print(f2.getvalue(), file=f_Save2)

        path_in_2 = r"file2.csv"
        path_out_2 = r"new_out_2.csv"

        d = DetectActualDate(path_in, path_out)
        d.call()

        d1 = DetectActualDate(path_in_1, path_out_1)
        d1.call()

        d2 = DetectActualDate(path_in_2, path_out_2)
        d2.call()

        self.result_0 = d.result
        self.result_1 = d1.result
        self.result_2 = d2.result

    def test_result(self):
        self.assertEqual(self.result_0['0987000001'], '2016-06-01')
        self.assertEqual(self.result_0['0987000002'], '2016-02-01')
        self.assertEqual(self.result_0['0987000003'], '2016-01-01')


        self.assertEqual(self.result_1['0987000001'], '2016-06-01')
        self.assertEqual(self.result_1['0987000002'], '2016-02-01')
        self.assertEqual(self.result_1['0987000003'], '2016-01-01')

        self.assertEqual(self.result_2['0987000001'], '2016-06-01')
        self.assertEqual(self.result_2['0987000002'], '2016-02-01')
        self.assertEqual(self.result_2['0987000003'], '2016-01-01')

if __name__ == '__main__':
    unittest.main(verbosity=2)


